package cis.camper.controller;

import cis.camper.entity.Camper;
import cis.camper.bo.CamperBO;
import cis.camper.dao.CamperDAO;
import cis.util.CisUtility;
import cis.util.CisUtilityFile;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A starting project which we can use for applications that need a menu driven
 * program. Note that the name of the project should be modified to reflect the
 * specific requirements.
 *
 * @author bjmaclean
 * @since 20181115
 */
public class Controller {

    public static final String EXIT = "X";

    private static final String MENU
            = "-------------------------\n"
            + "- CIS Menu\n"
            + "- A-Add\n"
            + "- B-View\n"
            + "- C-Edit (future functionality)\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";

    public static void main(String[] args) {

        //Add a loop below to continuously promput the user for their choice 
        //until they choose to exit.
        String option = "";

        do {
            option = CisUtility.getInputString(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase(EXIT));

    }

    /**
     * This method will process the menu option specified in the input
     * parameter. It will call appropriate functionality based on its value.
     *
     * @param option The menu option
     * @since 20171102
     * @author cis1201b
     *
     */
    public static void processMenuOption(String option) {
        //Add a switch to process the option
        switch (option.toUpperCase()) {
            case "A":
                System.out.println("Add a camper");

                if (CamperDAO.checkConnection()) {
                    addCamper();
                } else {
                    System.out.println("Can not add - database unavailable");
                }
                
                break;
            case "B":
                System.out.println("View all campers");
                viewAllCampers();
                break;
            case "C":
                CisUtility.display("Edit a camper");
                editACamper();
                break;
            case "GV":
                CisUtility.display(CisUtility.getRandom());
                break;
            case "X":
                CisUtility.display("User picked x");
                break;
            default:
                CisUtility.display("Invalid entry");
        }
    }

    /**
     * Add a camper and save to a file
     *
     * @since 2020-09-09
     * @author BJM
     */
    public static void addCamper() {

        Camper camper = new Camper();
        camper.getInformation();

        CamperBO camperBO = new CamperBO();
        camperBO.insert(camper);

    }

    /**
     * View all campers
     *
     * @since 20200923
     * @author BJM
     */
    public static void viewAllCampers() {
        CamperBO camperBO = new CamperBO();
        ArrayList<Camper> campers = camperBO.load();

        if (campers != null) {
            for (Camper current : campers) {
                System.out.println(current.toString());
            }
        }

    }

    public static void editACamper() {
        System.out.println("Future functionality");
    }

}
