package cis.camper.dao;

import cis.camper.entity.Camper;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Camper database access object
 *
 * @author bjm
 * @since 20200921
 */
public class CamperDAO {

    public static boolean checkConnection(){

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_camper",
                    "root",
                    ""); //--No password.  For assignment submission this would be expected to be empty.
        } catch (Exception e) {
            return false;
        }
        
        return true;

        
    }
    
    /**
     * Select the records from the database
     *
     * @since 20200923
     * @author BJM
     */
    public ArrayList<Camper> select() {
        ArrayList<Camper> campers = new ArrayList();

        //Select the campers from the database
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_camper",
                    "root",
                    ""); //--No password.  For assignment submission this would be expected to be empty.
        } catch (Exception e) {
            System.out.println("Could not make a connection to the database");
            return null;
        }

        try {

            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = statement.executeQuery("select * from Camper");

            //Show all the campers
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dob = rs.getString("dob");

                Camper camper = new Camper(String.valueOf(id), firstName, lastName, dob);
                campers.add(camper);

            }

        } catch (SQLException ex) {
            Logger.getLogger(CamperDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        return campers;

    }

    /**
     * Insert the camper into the database.
     *
     * @since 20200923
     * @author BJM
     *
     * 20200925 BJM Modifications to get running using standard password.
     */
    public Camper insert(Camper camper) {

        Connection conn = null;

        try {
            //BJM 20200925 Removed the password.
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_camper",
                    "root",
                    "");
        } catch (SQLException ex) {
            System.out.println("SQL exception happned!");
        }

        //***************************************************
        // INSERT
        //***************************************************
        try {
            String theStatement = "INSERT INTO Camper(firstName,lastName,dob) "
                    + "VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, camper.getFirstName());
            stmt.setString(2, camper.getLastName());
            stmt.setString(3, camper.getDob());

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

        return camper;
    }

}
