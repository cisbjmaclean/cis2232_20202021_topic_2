package cis.camper.bo;

import cis.camper.dao.CamperDAO;
import cis.camper.entity.Camper;
import java.util.ArrayList;

/**
 * Camper business object
 *
 * @author CIS2232
 * @since 20200923
 */
public class CamperBO {

    /**
     * Connect to the data access object to get the campers from the datasource.
     *
     * @since 20200923
     * @author BJM
     */
    public ArrayList<Camper> load() {

        //Read campers from the database
        CamperDAO camperDAO = new CamperDAO();
        ArrayList<Camper> campers = camperDAO.select();

        return campers;
    }
    
    public Camper insert(Camper camper){
        CamperDAO camperDAO = new CamperDAO();
        camper = camperDAO.insert(camper);
        return camper;
    }

}
