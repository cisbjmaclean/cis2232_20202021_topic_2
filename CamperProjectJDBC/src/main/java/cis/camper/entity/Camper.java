package cis.camper.entity;

import java.io.File;
import java.io.Serializable;
import java.util.Scanner;

/**
 * A camper
 *
 * @author bjm
 * @since 20200909
 */
public class Camper implements Serializable{

    private String idString, firstName, lastName, dob;

    public static final String FILE_LOCATION = "/cis"+File.separator
            +"cis2232"+File.separator+"topic1"+File.separator+"campers.json";

    public Camper() {
    }

    public Camper(String idString, String firstName, String lastName, String dob) {
        this.idString = idString;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
    }

    
    
    public Camper(String details) {
        String[] parts = details.split(",");
        this.idString = parts[0];
        this.firstName = parts[1];
        this.lastName = parts[2];
        this.dob = parts[3];

    }

    public void getInformation() {
        Scanner input = new Scanner(System.in);
        System.out.println("First Name:");
        firstName = input.nextLine();
        System.out.println("Last Name:");
        lastName = input.nextLine();
        System.out.println("dob:");
        dob = input.nextLine();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getIdString() {
        return idString;
    }

    public void setIdString(String idString) {
        this.idString = idString;
    }

    public String toString() {
        return idString + "," + firstName + "," + lastName + "," + dob;
    }

}
