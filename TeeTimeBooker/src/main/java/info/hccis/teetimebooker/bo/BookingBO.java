package info.hccis.teetimebooker.bo;

import com.google.gson.Gson;
import info.hccis.teetimebooker.Controller;
import info.hccis.teetimebooker.dao.BookingDAO;
import info.hccis.teetimebooker.entity.Booking;
import info.hccis.util.CisUtility;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class will contain the business logic concerned with accessing the
 * bookings.
 *
 * @since 2020-05-19
 * @author bjm
 */
public class BookingBO {

    public static final String PATH = "d:\\bookings\\";
    public static final String FILE_NAME = "bookings.json";

    private ArrayList<Booking> bookings = new ArrayList();

    /**
     * Load the bookings from the file into the bookings ArrayList.
     *
     * @since 2020-05-19
     * @author BJM
     */
    public  void loadBookings() {

        bookings.clear();
        BookingDAO bookingDAO = new BookingDAO();
        bookings = bookingDAO.selectAll();
    }

    /**
     * Write all bookings to the file
     *
     * @since 20200519
     * @author BJM
     */
    public void writeAllBookings() {

        //Create a new writer which will override the file containing the bookings.
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(PATH + FILE_NAME, false));
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Booking current : bookings) {
            try {
                //Use GSON to get a json string encoding for a booking
                Gson gson = new Gson();
                String bookingJson = gson.toJson(current);

                writer.write(bookingJson + System.lineSeparator());
            } catch (IOException ex) {
                CisUtility.display("Error with file access");
            }

        }
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Using java nio classes to ensure the bookings file is setup.
     *
     * @since 20200514
     * @author BJM
     */
    public void setupFile() {
        File myFile;
        try {
            Path path = Paths.get(PATH);
            try {
                Files.createDirectories(path);
            } catch (IOException ex) {
                CisUtility.display("Error creating directory");
            }

            myFile = new File(PATH + FILE_NAME);
            if (myFile.createNewFile()) {
                System.out.println("File is created!");
            } else {
                System.out.println("File already exists.");
            }

        } catch (IOException ex) {
            CisUtility.display("Error creating new file");
        }
    }

//    /**
//     * Update booking
//     *
//     * @since 2020-05-19
//     * @author BJM
//     */
//    public void updateBooking() {
//
//        loadBookings();
//
//        int id = CisUtility.getInputInt("What id do you want to update?");
//
//        //Identify which one to update...
//        for (Booking current : bookings) {
//            if (current.getId() == id) {
//                //found it!
//                CisUtility.display("Booking to update:");
//                CisUtility.display(current.toString());
//                CisUtility.display("Please provide new details");
//                current.getInformtion();
//                break;
//            }
//        }
//
//        //Rewrite all the bookings to the file.
//        writeAllBookings();
//    }

    /**
     * Read the file and show all the bookings
     *
     * @since 20200514
     * @author BJM
     */
    public void showBookings() {

        bookings.clear();
        loadBookings();

        for (Booking current : bookings) {
            CisUtility.display(current.toString());
        }

    }

    /**
     * Add a booking by writing to the database
     *
     * @since 20200514
     * @author BJM
     */
    public void addABooking() {
            Booking booking = new Booking();
            booking.getInformtion();

            BookingDAO bookingDao = new BookingDAO();
            bookingDao.insert(booking);
    }


    
}
