package info.hccis.teetimebooker.entity;

import info.hccis.util.CisUtility;
import java.io.Serializable;

/**
 * Booking class
 *
 * @author bjm
 * @since 20200514
 */
public class Booking implements Serializable{

    private int id;
    private String name1, name2, name3, name4;
    private String courseName;
    private String dateOfBooking;
    private String timeOfBooking;

    private static int nextId = 0;

    public Booking() {
        
    }

    public Booking(int id, String name1, String name2, String name3, String name4, String courseName, String dateOfBooking, String timeOfBooking) {
        this.id = id;
        this.name1 = name1;
        this.name2 = name2;
        this.name3 = name3;
        this.name4 = name4;
        this.courseName = courseName;
        this.dateOfBooking = dateOfBooking;
        this.timeOfBooking = timeOfBooking;
    }

    /**
     * Set the next id for a new Booking
     * @since 2020-05-19
     * @author BJM
     */
    
    public int setNextId(){
        id = ++nextId;
        return id;
    }
    
    public static int getNextId() {
        return nextId;
    }

    public static void setNextId(int nextId) {
        Booking.nextId = nextId;
    }

    public void getInformtion() {
        name1 = CisUtility.getInputString("Name of booker:");
        name2 = CisUtility.getInputString("Player 2:");
        name3 = CisUtility.getInputString("Player 3:");
        name4 = CisUtility.getInputString("Player 4:");
        courseName = CisUtility.getInputString("Course name:");
        dateOfBooking = CisUtility.getInputString("Date of booking (yyyy-MM-dd) or enter to use current date");
        if (dateOfBooking.isEmpty()) {
            dateOfBooking = CisUtility.getCurrentDate("yyyy-MM-dd");
        }
        timeOfBooking = CisUtility.getInputString("Time of booking (hh:mm)");

    }

    public void fromCSV(String csv) {
        String[] parts = csv.split(",");
        this.id = Integer.parseInt(parts[0]);
        this.dateOfBooking = parts[1];
        this.courseName = parts[2];
        this.name1 = parts[3];
        this.name2 = parts[4];
        this.name3 = parts[5];
        this.name4 = parts[6];
    }

    public String toCSV() {
        return this.id + ","
                + this.dateOfBooking + ","
                + this.courseName + ","
                + this.name1 + ","
                + this.name2 + ","
                + this.name3 + ","
                + this.name4;

    }

    public String getTimeOfBooking() {
        return timeOfBooking;
    }

    public void setTimeOfBooking(String timeOfBooking) {
        this.timeOfBooking = timeOfBooking;
    }
 
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getName3() {
        return name3;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }

    public String getName4() {
        return name4;
    }

    public void setName4(String name4) {
        this.name4 = name4;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDateOfBooking() {
        return dateOfBooking;
    }

    public void setDateOfBooking(String dateOfBooking) {
        this.dateOfBooking = dateOfBooking;
    }

    public String toString() {
        return id + ") Date: " + dateOfBooking + " Course: " + courseName + " Name:" + name1 + " (" + name2 + "," + name3 + "," + name4 + ")";
    }

}
